import { CustomerOrderLine } from "./CustomerOrderLine";
import { OrderType } from "./OrderType";

export interface CustomerOrder{
  orderLine: CustomerOrderLine[],
  type: OrderType
}
