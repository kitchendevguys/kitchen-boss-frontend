export interface Dish {
  id?: number;
  name?: string;
  description?: string;
  price?: number;
  allergenes?: string[];
  category?: string;
}
