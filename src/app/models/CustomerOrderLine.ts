export interface CustomerOrderLine{
  foodId: number,
  qty: number
}
