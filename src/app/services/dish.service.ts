/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-useless-constructor */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { apiconfig } from '../api-config';
import { Allergene } from '../models/Allergene';
import { CustomerOrder } from '../models/CustomerOrder';
import { Dish } from '../models/Dish';
import { DishCategory } from '../models/DishCategory';
import { Order } from '../models/Order';

@Injectable({
  providedIn: 'root'
})
export class DishService {
  constructor(private http: HttpClient) {}
  mockAllergenes: string[] = [
    'répa',
    'retek',
    'mogyoró'
  ];
  mockCategories = [
    'Desszert', 'Főétel', 'Leves', 'Pizza'
  ];
  mockDishes: Dish[] = [
    {
      id: 4,
      name: 'Étel4',
      description: 'leírás 4',
      category: this.mockCategories[0],
      price: 1000,
      allergenes: ['répa, retek, mogyoró']
    },
    {
      id: 1,
      name: 'Étel1',
      description: 'leírás 1',
      category: this.mockCategories[0],
      price: 1000,
      allergenes: ['répa, retek, mogyoró']
    },
    {
      id: 2,
      name: 'Étel2',
      description: 'leírás 1',
      category: this.mockCategories[1],
      price: 1000,
      allergenes: ['répa, retek, mogyoró']
    },
    {
      id: 3,
      name: 'Étel3',
      description: 'leírás 1',
      category: this.mockCategories[2],
      price: 1000,
      allergenes: ['répa, retek, mogyoró']
    },
  ];

  
  registryDish(restaurantId: number, newDish: Dish): Observable<any> {
    // return of("oksa");
    return this.http.post<any>(
      apiconfig.apiUrl + "/restaurants/"+restaurantId+"/food",
      newDish);
  }

  getRestaurantMenu(id: number): Observable<Dish[]> {
    // return of(this.mockDishes);
    return this.http.get<Dish[]>(
        apiconfig.orderApiUrl + "/restaurants/"+id+"/food");
  }

  getAllergenes(): Observable<string[]> {
    // return of(this.mockAllergenes);
    return this.http.get<string[]>(apiconfig.apiUrl + "/food/allergenes");
  }

  getDishCategories(): Observable<string[]>{    
    // return of(this.mockCategories);
    return this.http.get<string[]>(apiconfig.apiUrl + "/food/categories");
  }

  order(restaurantId: number, order: CustomerOrder): Observable<any>{
    console.log("RestaurantId: "+restaurantId ,"Order: ",order);
    // return of("ok");

    return this.http.post<any>(
      apiconfig.orderApiUrl+"/restaurants/"+restaurantId+"/orders", 
      order
    );
  }

  modifyDishPrice(resturantId: number, dishId: number, newPrice: number): Observable<any>{
    // return of("ok");

    return this.http.put<any>(apiconfig.orderApiUrl + "/restaurants/"+resturantId+"/food",
    {
      foodId: dishId,
      price: newPrice
    });
  }


}
