import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { apiconfig } from '../api-config';
import { Delivery } from '../models/Delivery';
import { Order } from '../models/Order';
import { OrderService } from './order.service';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {
  mockDeliveries: Delivery[] = [
    {
      id: 1,
      firstName: 'Gergő',
      lastName: ''
    },
    {
      id: 2,
      firstName: 'Bálint',
      lastName: ''
    }
  ]; 

  constructor(private http: HttpClient, private orderService: OrderService) {  }

  getAllDeliveryWorkers(): Observable<Delivery[]>{
    // return of(this.mockDeliveries);
    
    return this.http.get<Delivery[]>(apiconfig.orderApiUrl+"/delivery/all");
  }

  getAssignedJobs(): Observable<Order[]>{
    // return of(this.orderService.mockOrders);
    return this.http.get<Order[]>(
      apiconfig.orderApiUrl+"/delivery/current-jobs"
    );
  }


  changeJobStatus(orderChangeObject: any): Observable<any>{
    // return of("oksa");
    return this.http.post<any>(
      apiconfig.orderApiUrl+"/delivery/job-status", 
      orderChangeObject
    );
  }

}
