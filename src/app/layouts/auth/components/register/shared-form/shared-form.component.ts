import { Component, OnInit, forwardRef, Input, OnDestroy } from '@angular/core';
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validators
} from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, map ,distinctUntilChanged} from 'rxjs/operators';
import { Address } from 'src/app/models/Address';
import { AuthService } from 'src/app/services/auth.service';
export interface SharedFormValues {
  username: string;
  password: string;
  passwordMatch: string;
  email: string;
  address: string;
}

@Component({
  selector: 'app-shared-form',
  templateUrl: './shared-form.component.html',
  styleUrls: ['./shared-form.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SharedFormComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SharedFormComponent),
      multi: true
    }
  ]
})
export class SharedFormComponent
  implements OnInit, ControlValueAccessor, OnDestroy {
  @Input()
  addressControlName!: string;
  
  
  addressChanged: Subject<string> = new Subject<string>();
  address:FormControl =  new FormControl('');
  addressRecommendations: Address[] = [];
  addressString: string = "";

  sharedForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    passwordMatch: new FormControl(''),
    email: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    placeId: this.address
  });
  subscriptions: Subscription[] = [];
  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {}

  ngOnInit(): void {
    this.sharedForm = this.formBuilder.group({
      username: [
        null,
        // [Validators.required, Validators.minLength(6), Validators.maxLength(12)]
      ],
      password: [
        null,
        // [Validators.required, Validators.minLength(8), Validators.maxLength(16)]
      ],
      passwordMatch: [
        null,
        // [Validators.required, Validators.minLength(8), Validators.maxLength(16)]
      ],
      email: [null],
      firstName: [null],
      lastName: [null],
      placeId: [null]
      // email: [null, [Validators.required, Validators.email]],
      // firstName: [null, [Validators.required]],
      // lastName: [null, [Validators.required]],
      // address: [null, [Validators.required]]
    });
    this.initSubs();
    this.addressChanged.pipe(
      debounceTime(500),
      distinctUntilChanged(),
    ).subscribe((address: string)=>{
      console.log("GOOGLE API REQUEST");
      this.authService.searchAddress(address)
        .subscribe((addresses: Address[])=>{
          console.log(addresses);
          this.addressRecommendations = addresses;
        });
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  get value(): SharedFormValues {
    return this.sharedForm.value;
  }

  set value(value: SharedFormValues) {
    this.sharedForm.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  initSubs(): void {
    this.subscriptions.push(
      this.sharedForm.valueChanges.subscribe((value) => {
        this.onChange(value);
        this.onTouched(true);
      })
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  validate(_: FormControl): any {
    return this.sharedForm.valid ? null : { profile: { valid: false } };
  }

  // Implementing the ControlValueAccessor API
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  writeValue(value: any): any {
    if (value) {
      this.value = value;
    } else {
      this.sharedForm.reset();
    }
  }
  // Implementing the ControlValueAccessor API
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  // Implementing the ControlValueAccessor API
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  searchAddress(address: string): void {
    console.log("address...");
    
  }

  changeAddress(address: string): void{
    this.addressChanged.next(address);
    this.sharedForm.controls['placeId'].setValue(this.addressString);
  }
}

