import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';

import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';

import { AppRoutingModule } from './app-routing.module';

import { AuthInterceptor } from './services/auth.interceptor';

import { AppComponent } from './app.component';

import { NzMessageModule } from 'ng-zorro-antd/message';

import { NzSelectModule } from 'ng-zorro-antd/select';

registerLocaleData(en);

@NgModule({
  bootstrap: [AppComponent],
  declarations: [AppComponent],
  imports: [
    // - Angular
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    // * We need to import "HttpClientModule" to use dynamic importing (NG-ZORRO [icons-provider.module.ts])
    HttpClientModule,
    // - Routing
    AppRoutingModule,
    // - NG-ZORRO
    NzLayoutModule,
    NzDrawerModule,
    NzGridModule,
    NzTypographyModule,
    NzButtonModule,
    NzFormModule,
    NzInputModule,
    NzIconModule,
    NzMessageModule,
    NzSelectModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ]
})
export class AppModule {}
