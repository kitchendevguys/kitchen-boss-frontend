import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth-guard';
import { Role } from './models/Role';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./layouts/home/home.module').then((m) => m.HomeModule)
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./layouts/auth/auth.module').then((m) => m.AuthModule)
  },
  {
    path: 'owner',
    loadChildren: () =>
      import('./layouts/owner/owner.module').then((m) => m.OwnerModule),
    canActivate: [AuthGuard],
    data: {
      roles: [Role.OWNER]
    }
  },
  {
    path: 'customer',
    loadChildren: () =>
      import('./layouts/customer/customer.module').then((m) => m.CustomerModule)
  },
  {
    path: 'delivery',
    loadChildren: () =>
      import('./layouts/delivery/delivery.module').then((m) => m.DeliveryModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
