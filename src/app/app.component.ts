import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject } from 'rxjs';
import { Role } from './models/Role';
import { AuthService } from './services/auth.service';
import { CartService } from './services/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  shoppingCartDrawerVisibility = new BehaviorSubject(false);
  shoppingCartDrawerVisibility$ = this.shoppingCartDrawerVisibility.asObservable();


  links = [
    {
      path: '/home',
      title: 'Éttermek',
      roles: [Role.CUSTOMER, Role.DELIVERY, Role.OWNER, Role.ANYONE]
    },
    {
      path: '/owner/add-dish',
      title: 'Étlap szerkersztése',
      roles: [Role.OWNER]
    },
    // {
    //   path: '/owner/dish-list',
    //   title: 'Ételek listázása',
    //   roles: [Role.OWNER]
    // }, // NEM KELL MERT AZ OWNER AZ ÉTTERMEK FÜLÖN KIVÁLASZTJA AZ ÉTTERMEI KÖZÜL HOGY MELYIK ÉTELEIT AKARJA MEGNZÉNI
    {
      path: '/customer/order',
      title: 'Rendelés',
      roles: [Role.CUSTOMER]
    },
    {
      path: '/owner/orders',
      title: 'Bejövő rendelések',
      roles: [Role.OWNER]
    },
    {
      path: '/delivery/jobs',
      title: 'Kiszállítási munkák',
      roles: [Role.DELIVERY]
    }
  ];

  constructor(public authService: AuthService, public cartService: CartService, private router: Router) {}
  ngOnInit(): void {}

  toggleDrawer(): void {
    const prevState = this.shoppingCartDrawerVisibility.getValue();
    this.shoppingCartDrawerVisibility.next(!prevState);
  }

  linkShow(link: any, role: Role): boolean {
    return link.roles.includes(role);
  }

  onOrder(): void{
    console.log("fizetés");
    this.router.navigate(['/customer/order']);
    this.toggleDrawer();
  }
}
